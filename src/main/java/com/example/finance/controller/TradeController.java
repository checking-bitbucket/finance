package com.example.finance.controller;

import com.example.finance.entity.Trade;
import com.example.finance.exception.ResourceNotFoundException;
import com.example.finance.repository.TradeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/trades")
@CrossOrigin
public class TradeController {

    @Autowired
    private TradeRepository tradeRepository;

    @GetMapping(value = "/")
    public ResponseEntity<List<Trade>> allTheTrades() {
        List<Trade> trades = (List<Trade>) tradeRepository.findAll();
        return new ResponseEntity<>(trades, HttpStatus.OK);
    }

    @GetMapping(value = "/{tradeID}")
    public ResponseEntity<Trade> tradeById(@PathVariable("tradeID") int tradeID)
            throws ResourceNotFoundException {

        Trade trade = tradeRepository.findById(tradeID)
                .orElseThrow(() -> new ResourceNotFoundException("Trade with id: " + tradeID + " does not exist"));
        return new ResponseEntity<>(trade,HttpStatus.OK);
    }

    @PostMapping(value = "/", produces = "application/json")
    public ResponseEntity<Trade> addNewTrade(@RequestBody Trade trade) {
        return new ResponseEntity<>(tradeRepository.save(trade), HttpStatus.OK);
    }

    @PutMapping(value = "/{tradeID}")
    @ResponseBody
    public ResponseEntity<Trade> editByTradeID(@PathVariable("tradeID") int tradeID, @RequestBody Trade trade) throws ResourceNotFoundException {

        Trade myTrade = tradeRepository.findById(tradeID)
                .orElseThrow(() -> new ResourceNotFoundException("Trade with id:  " + tradeID + " does not exist"));
        myTrade.setPrice(trade.getPrice());
        myTrade.setBuyOrSell(trade.getBuyOrSell());
        myTrade.setVolume(trade.getVolume());
        myTrade.setStatusCode(trade.getStatusCode());
        myTrade.setStockTicker(trade.getStockTicker());
        return new ResponseEntity<>(tradeRepository.save(myTrade), HttpStatus.OK);
    }

    @DeleteMapping(value="/{tradeID}")
    public ResponseEntity<Trade> deleteById(@PathVariable("tradeID") int tradeID) throws ResourceNotFoundException {
        Trade trade = tradeRepository.findById(tradeID)
                .orElseThrow(() -> new ResourceNotFoundException("Trade with id: " + tradeID + " does not exist"));
        tradeRepository.delete(trade);
        return ResponseEntity.ok().build();

    }
}