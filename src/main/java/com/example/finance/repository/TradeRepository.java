package com.example.finance.repository;

import com.example.finance.entity.Trade;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface TradeRepository extends CrudRepository<Trade,Integer> {
}
